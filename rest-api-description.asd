(asdf:defsystem #:rest-api-description
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Artistic 2.0"
  :serial t
  :components ((:module "code" :components
                        ((:file "package")
                         (:file "utils")
                         (:file "protocol")
                         (:file "simple-parameter")
                         (:file "token-header")
                         (:file "simple-route")
                         (:file "simple-endpoint")))))
