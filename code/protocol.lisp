#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description)

;;; api
(defclass api () ()
  (:documentation "An api defines a collection of routes, a route may belong to
more than one api.

This is to be used by a generator to find all the routes belonging to an api
that are *reachable*."))

;;; route
(defclass route () ())

;;; TODO this is being refactored, i don't think routes have any kind of
;;; parameters except route ones? not sure yet though
;;; i think routes can have query parmaeters amoung other things
;;; as well as endpoints having more specific ones.
;;; What is certain is that routes cannot have body parameters
;;; only endpoints with a method that supports body parameters.
(defgeneric find-parameters (appears-in route)
  (:documentation "Returns all the parameters that appear in this thing."))

(defgeneric route-composition (route)
  (:documentation "Returns a list of constant and parameter componenets of a route
in addition to other routes that make up the route."))

(defgeneric route-parameters (route)
  (:documentation "Return ALL the parameters in the route."))

(defgeneric route-endpoints (route)
  (:documentation "Return ALL the endpoints reachable via this
exact route."))

(defgeneric add-route-endpoint (route endpoint)
  (:documentation "Register the endpoint as reachable to the route"))

;;; parameters
(defclass parameter () ())
(defclass named-parameter () ())

(defgeneric serial-name (parameter)
  (:documentation "A String name for the parameter in it's serial form
If the parameter is named."))

(defgeneric appears-in (parameter)
  (:documentation "Where the parameter appear-in."))

;;; endpoints
(defclass endpoint () ())
(defgeneric endpoint-method (endpoint))
(defgeneric endpoint-route (endpoint))

;;; in the generator, would it be possible to have this notion of a package
;;; that you choose an implementation of?
;;; this is to try ie choose drakma-async on some platforms
;;; and cl-http on others etc.

;;;; do parameters need a :type that is a symbol so that generators can
;;;; implement a generic taking a type and a seiral string
;;;; and try coerce them or whatever and then people can have
;;;; extensible types.
