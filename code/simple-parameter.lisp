#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description)

(defclass simple-parameter (parameter)
  ((%name :initarg :name
          :reader name
          :type symbol)
   (%appears-in :initarg :appears-in
                :reader appears-in
                :type symbol)))

(defclass simple-named-parameter (named-parameter simple-parameter)
  ((%serial-name :initarg :serial-name
                 :reader serial-name
                 :type string)))

(defgeneric make-parameter (name appears-in &rest initargs)
  (:method (name appears-in &rest initargs)
    (declare (ignore initargs))
    (make-instance 'simple-parameter
                   :name name
                   :appears-in appears-in)))

(defmacro define-simple-named-parameter-method (appears-in)
  `(defmethod make-parameter  (name (appears-in (eql ,appears-in)) &rest initargs
                               &key serial-name &allow-other-keys)
     (declare (ignore initargs))
     (let ((serial-name (or serial-name
                            (string-downcase name))))
       (make-instance 'simple-named-parameter
                      :name name
                      :appears-in appears-in
                      :serial-name serial-name))))

(define-simple-named-parameter-method :body)
(define-simple-named-parameter-method :query)

(defmethod make-parameter (name (appears-in (eql :header))
                           &rest initargs
                           &key (header-class 'simple-header-parameter)
                           &allow-other-keys)
  (remf initargs :header-class)
  (apply #'make-instance header-class (list* :appears-in appears-in initargs)))

(defclass simple-header-parameter (simple-parameter)
  ())
