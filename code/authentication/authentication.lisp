#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description.authentication)

(defclass authentication ()
  ((%origin :initarg :origin
            :reader origin
            :documentation "RFC 6454 Section 4.")))

(defclass token-authentication (authentication)
  ((%access-token :initarg :access-token :reader access-token)))
