(asdf:defsystem #:rest-api-description.authentication
    :author "Gnuxie <Gnuxie@protonmail.com>"
    :serial t
    :components ((:file "package")
                 (:file "authentication")))
