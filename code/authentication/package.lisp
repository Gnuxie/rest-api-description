#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#
;;;; This module provides an abstract authentication componenet
;;;; that client generators can use.
(defpackage #:rest-api-description.authentication
  (:use #:cl)
  (:export
   #:authentication
   #:origin

   #:token-authentication
   #:access-token))
