#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description)

(defclass simple-route (route)
  ((%route-composition :initarg :route-composition
                       :reader route-composition
                       :type list
                       :documentation
                       "this is in reality a cached field that is generated
once the immediate-composition is finalized.")

   (%immediate-parameters :initarg :immediate-parameters
                          :reader immediate-parameters
                          :type list)

   (%name :initarg :name
          :reader name
          :type symbol
          :documentation "The name of the route")

   (%endpoints :initarg :endpoints
               :reader route-endpoints
               :accessor %endpoints
               :type list
               :initform '())))

(defmethod route-parameters ((route simple-route))
  (let ((resultant-alist (immediate-parameters route)))
    (loop :for component :in (route-composition route)
       :when (typep component 'route)
       :do (loop :for parameter-assoc :in (route-parameters component)
              :do (destructuring-bind (key &rest parameter-set) parameter-assoc
                    (let ((entry (assoc key resultant-alist)))
                      (if entry
                          (setf (cdr entry) (append (cdr entry) parameter-set))
                          (push (cons key parameter-set) resultant-alist))))))
    resultant-alist))

(defparameter *route-table* (make-hash-table))

(defun find-route (symbol &optional (errorp t))
  (let ((entry (gethash symbol *route-table*)))
    (if errorp
        (or entry
            (error "Can't find route with name ~a" symbol))
        entry)))

(defun calculate-composition (immediate-composition immediate-route-parameters)
  (mapcar
   (lambda (componenet)
     (etypecase componenet
       (string componenet)
       (symbol
        (let ((route-entry (find-route componenet nil))
              (route-parameter (find componenet immediate-route-parameters
                                     :key #'name)))
          (or route-entry
              route-parameter
              (error "componenet ~a is not a route or a parameter" componenet))))))
   immediate-composition))

;;; now we've got this problem where route-parameters doesn't return parameters
;;; from it's componenets, not sure if it's the purpose of the client
;;; to figure that out yet or not.
(defun make-route (name &key composition parameters)
  (let* ((grouped-parameters (group #'appears-in parameters))
         (route
          (make-instance
           'simple-route
           :name name
           :immediate-parameters grouped-parameters
           :route-composition
           (calculate-composition composition
                                  (cdr (assoc :route grouped-parameters))))))
    (setf (gethash name *route-table*) route)))

(defmethod find-parameters (appears-in (route simple-route))
  (cdr (assoc appears-in (route-parameters route))))

;;; maybe the parameter or make-parameter thing should use something like
;;; the method in c2mop that finds a slot-class depending on the initargs
;;; that way we can ensure that parameters are extensible
(defmacro define-route (name (&rest composition)
                                (&rest parameters)
                        &rest other-arguments)
  (let* ((parameter-forms
          (loop :for parameter :in parameters
             :collect (destructuring-bind (symbol-name appears-in &rest initargs)
                          parameter
                        `(make-parameter ',symbol-name ',appears-in
                                         ,@ initargs))))
         (arguments
          (append (and parameter-forms
                       (list :parameters `(list ,@parameter-forms)))
                  other-arguments)))
    `(progn
       (make-route ',name
                   :composition ',composition
                   ,@arguments))))

(defgeneric package-routes (package)
  (:method (package)
    (let ((routes '()))
      (do-external-symbols (symbol package)
        (let ((route (find-route symbol nil)))
          (when route
            (push route routes))))
      routes)))

(defmethod add-route-endpoint ((route simple-route) endpoint)
  (pushnew endpoint (%endpoints route)))
