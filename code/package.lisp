#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:rest-api-description
  (:use #:cl)
  (:export
   ;; protocol.lisp
   #:api
   #:route
   #:find-parameters
   #:route-composition
   #:route-parameters
   #:route-endpoints
   #:parameter
   #:named-parameter
   #:serial-name
   #:appears-in
   #:name
   #:package-routes

   ;; endpoints
   #:endpoint
   #:endpoint-method
   #:endpoint-route
   #:define-endpoint
   ;; simple-parameter.lisp
   #:make-parameter
   #:token-header
   #:access-token-header
   #:header-name
   #:format-string
   ;; simple-route.lisp
   #:make-route
   #:find-route
   #:define-route))
