#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description)

;;; TODO
;;; Is the token header going to be added to the parameter list of a
;;; generated function?
;;; We know the access-token-header will not be added to a paramter list
;;; so this is pretty complicated.
(defclass token-header (simple-header-parameter)
  ((%header-name :initarg :header-name :reader header-name
                 :type string :initform "Authorization")
   (%format-string :initarg :format-string :reader format-string
                   :type string :initform "Bearer ~a")))

(defclass access-token-header (token-header)
  ())
