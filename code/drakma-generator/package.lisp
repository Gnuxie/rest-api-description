#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:rest-api-description.drakma-generator
  (:use #:cl)
  (:local-nicknames
   (#:rad #:rest-api-description)
   (#:auth #:rest-api-description.authentication))
  (:export
   #:define-api
   #:ensure-package

   #:config
   #:wrapper-function
   #:wrap-request<-route
   #:uri-constructor<-route-description))
