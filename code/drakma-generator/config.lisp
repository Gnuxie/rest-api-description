#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description.drakma-generator)

(defun ensure-package (package-designator)
  (let ((package (find-package package-designator)))
    (or package
        (defpackage-plus-1:ensure-package package-designator))))

(defclass config ()
  ((%target-package :initarg :target-package
                    :reader target-package
                    :type package)

   (%wrapper-function :initarg :wrapper-function
                      :reader wrapper-function
                      :type function
                      :initform  (lambda (config route form)
                                   (declare (ignore config route))
                                   form))))
