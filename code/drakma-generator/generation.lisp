#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description.drakma-generator)

;;; TODO
;;; WE need to know if the wrapper idea is going to create much larger functions
;;; and how to impove that if it's the case
;;; one way is to allow replacement of the drakma:http-request
;;; compeltly in place of another function.

;;; TODO
;;; these symbol interning things should be moved to another file.
(defgeneric target-symbol<-rad-name (config rad-name)
  (:method ((config config) (rad-name symbol))
    (intern (symbol-name rad-name) (target-package config))))

(defgeneric keyword-entry<-parameter (config parameter)
  (:method ((config config) (parameter rad:parameter))
    (target-symbol<-rad-name config (rad:name parameter))))

(defgeneric parameter-list<-route-description (config method route)
  (:method ((config config) (method (eql :get)) (route rad:route))
    (append
     '(authentication)
     (loop :for parameter :in (rad:find-parameters :route route)
        :collect (target-symbol<-rad-name config (rad:name parameter)))
     '(&key)
     (loop :for parameter :in (rad:find-parameters :query route)
        :collect (keyword-entry<-parameter config parameter)))))

(defgeneric complete-composition (config route)
  (:method ((config config) (route rad:route))
    (let ((immediate-composition (rad:route-composition route)))
      (loop :for component :in immediate-composition
         :appending
           (etypecase component
             (rad:route (complete-composition config component))
             (rad:parameter (list component))
             (string (list component)))))))

(defgeneric uri-constructor<-route-description (config route)
  (:method ((config config) (route rad:route))
    `(format nil
             ,(apply #'concatenate
                     'string
                     "~a" ; origin from authentication.
                     (loop :for component :in (complete-composition config route)
                        :collect (typecase component
                                   (string component)
                                   (t "~a"))))
             (auth:origin authentication)
             ,@ (loop :for component :in (complete-composition config route)
                   :when (typep component 'rad:parameter)
                   :collect (target-symbol<-rad-name config (rad:name component))))))

(defgeneric drakma-parameters<-route (config route)
  (:method ((config config) (route rad:route))
    `(append
      ,@ (loop :for parameter :in (rad:find-parameters :query route)
            :collect (let ((target-symbol
                            (target-symbol<-rad-name config (rad:name parameter))))
                       `(and ,target-symbol
                             ,``((,,(rad:serial-name parameter) . ,,target-symbol))))))))

;;; TODO
;;; we need to come up with come common protocol between a drakma and a dexador or whatever
;;; http backend, what i mean by this is that we have generators that generate
;;; the same definitions and have the same wrapping mechanism
;;; it's not really a priority though.
(defgeneric wrap-request<-route (config route-method route-name route request-form)
  (:documentation "The name as added to the arguments so that it is possible
to override specific behavoir per route and method.")
  (:method ((config config) route-method route-name (route rad:route) request-form)
    (declare (ignore route-method route-name))
    (funcall (wrapper-function config) config route request-form)))

(defgeneric function<-route-description (config method endpoint route)
  (:method ((config config) (method (eql :get)) endpoint (route rad:route))
    (declare (ignore endpoint))
    (let ((function-name (target-symbol<-rad-name config (rad:name route))))
      `(progn
         (defpackage-plus-1:ensure-export
          '(,function-name) (find-package ',(package-name (target-package config))))
         (defun ,function-name
             ,(parameter-list<-route-description config method route)
           ,(wrap-request<-route
             config method (rad:name route) route
             `(drakma:http-request
               ,(uri-constructor<-route-description config route)
               :parameters ,(drakma-parameters<-route config route)
               :additional-headers (list ,@(additional-headers<-route config route)))))))))

(defgeneric api-bindings<-package (config definition-package)
  (:method ((config config) definition-package)
    `(progn
       ;; pretty sure the ensure package has to be in a seperate compilation unit
       (eval-when (:compile-toplevel :load-toplevel :execute)
         (ensure-package ',(package-name (target-package config))))
       ,@ (loop :for route :in (rad:package-routes definition-package)
             :append (loop :for endpoint :in (rad:route-endpoints route)
                           :collect (function<-route-description
                                     config (rad:endpoint-method endpoint)
                                     endpoint route))))))
