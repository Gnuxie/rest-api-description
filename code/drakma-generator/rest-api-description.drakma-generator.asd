(asdf:defsystem #:rest-api-description.drakma-generator
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :serial t
  :depends-on ("rest-api-description" "rest-api-description.authentication" "drakma"
                                      "defpackage-plus")
  :components ((:file "package")
               (:file "config")
               (:file "header")
               (:file "generation")
               (:file "macro")))
