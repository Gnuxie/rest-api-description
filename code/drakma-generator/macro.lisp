#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description.drakma-generator)

;;; 'wrapper function' should not exist
;;; instead, the macro should accept a class-name and additional
;;; initialization arguments, so that the behavoir of the generator
;;; can be easily overriden.

;;; remember, we put the message into the tube in the wrapped area
;;; this is where we need to put the url and any extra stuff.
(defmacro define-api (target-package description-package
                      &key (generator 'config)
                        wrapper-function)
  (let* ((target-package (ensure-package target-package))
         (config (if wrapper-function
                     (make-instance generator
                                    :target-package target-package
                                    :wrapper-function
                                    (etypecase wrapper-function
                                      (function wrapper-function)
                                      (symbol (fdefinition wrapper-function))))
                     (make-instance generator
                                    :target-package target-package))))
    (api-bindings<-package config description-package)))
