#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description.drakma-generator)

;;; TODO
;;; how to keep track of the 'leaky' 'authentication' symbol?

;;; it seems to me like these generics are all randomly named with no order
;;; structure to them, how is someone supposed to find out their utility
;;; other than inspecting them (which fortunatley most of them are extreemly simple)
;;; one way I can think of at the moment is to shadow closely to the
;;; rad protocol somehow, but also we still havn't finished this module yet
;;; and we're likely to develop a protocol for all client generators to implement
;;; this would add some structure at least, we need to find out what that is first though.
;;; so i wouldn't be too concerned about it yet, we just have to do the hard work.
(defgeneric header-alist-constructor<-header (config route header-parameter)
  (:method ((config config) (route rad:route) (header rad:token-header))
    (declare (ignore route))
    `(cons ',(rad:header-name header)
           (format nil ,(rad:format-string header)
                   (auth:access-token authentication)))))

(defgeneric additional-headers<-route (config route)
  (:method ((config config) (route rad:route))
    (let ((header-parameters (rad:find-parameters :header route)))
      (loop :for param :in header-parameters
            :collect (header-alist-constructor<-header config route param)))))
