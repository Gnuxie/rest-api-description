#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description)

(defun group (key-function list &key (test #'eql))
  (let ((key-item-alist '()))
    (mapc
     (lambda (item)
       (let* ((key (funcall key-function item))
              (entry (assoc key key-item-alist :test test)))
         (if entry
             (push item (cdr entry))
             (push (list key item) key-item-alist))))
     list)
    key-item-alist))
