(asdf:defsystem #:infernal-pneumatic-wraps
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Artistic License 2.0"
  :depends-on ("infernal-pneumatics" "rest-api-description.drakma-generator")
  :serial t
  :components ((:file "package")
               (:file "wraps")))
