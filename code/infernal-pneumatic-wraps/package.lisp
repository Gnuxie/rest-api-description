#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:infernal-pneumatic-wraps
  (:use #:cl)
  (:local-nicknames
   (#:ip #:infernal-pneumatics)
   (#:dg #:rest-api-description.drakma-generator)
   (#:rad #:rest-api-description))
  (:export
   #:*resource-loader-tube*
   #:infernal-pneumatic-drakma
   #:api-call-tube
   #:api-call
   #:api-call-event
   #:route-name
   #:request-uri

))
