#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:infernal-pneumatic-wraps)

;;; for now we can report using the function and args tbh.
(defclass api-call (ip:blackbird-function-message)
  ((%request-uri :initarg :request-uri :reader request-uri)
   (%route-name :initarg :route-name :reader route-name)))

;;; is the only reason we're doing this is so that we can
;;; share the resource-loader-mailbox between libraries??
;;; and so share the same thread pool?
;;; yes.

;;; infernal pneumatic wraps should actually override the generator.

(defclass api-call-tube (ip:safe-queue-tube)
  ())

(defclass api-call-event (ip:received-message-event)
  ((%request-uri :initarg :request-uri :reader request-uri)
   (%route-name :initarg :route-name :reader route-name)))

(defvar *resource-loader-tube* (make-instance 'api-call-tube))

(defclass infernal-pneumatic-drakma (dg:config)
  ())

(defmethod ip:received-message ((tube api-call-tube)
                                (message api-call))
  (ip:safe-queue-event tube message 'api-call-event
                       :request-uri (request-uri message)
                       :route-name (route-name message)))

(defmethod ip:calculate-special-bindings ((tube api-call-tube) &rest arguments
                                          &key &allow-other-keys)
  (declare (ignore arguments))
  (let ((bindings (call-next-method)))
    (pushnew (cons '*resource-loader-tube* *resource-loader-tube*) bindings :test
             #'equal)
    bindings))

(defmethod dg:wrap-request<-route
    ((generator infernal-pneumatic-drakma) route-name route-method (route rad:route) request-form)
  (declare (ignore route-method route-name request-form))
  `(ip:send *resource-loader-tube*
            (make-instance 'api-call
                           :request-uri
                           ,(dg:uri-constructor<-route-description
                             generator route)
                           :route-name ',(rad:name route)
                           :function
                           (lambda ()
                             ,(call-next-method)))))
