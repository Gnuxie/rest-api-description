#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:rest-api-description)

(defclass simple-endpoint (endpoint)
  ((%route :initarg :route :reader endpoint-route)
   (%method :initarg :method :reader endpoint-method)))

(defun make-endpoint (method route)
  (let ((endpoint
          (make-instance 'simple-endpoint
                         :method method
                         :route route)))
    (add-route-endpoint route endpoint)
    endpoint))

(defmacro define-endpoint ((method route-designator))
  `(make-endpoint ',method (find-route ',route-designator)))
